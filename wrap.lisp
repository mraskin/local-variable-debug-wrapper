(defpackage
  :local-variable-debug-wrapper
  (:use :common-lisp
        #-(or ecl clisp) :hu.dwim.walker
        #+(or ecl clisp) :local-variable-debug-wrapper-lexenv-parser
        )
  (:export
    :locvar :locfunc :local-variable :local-function
    :list-local-variables :list-local-functions
    :list-locvars :list-locfuncs
    :lexenv-stack-cursor-set
    :with-locals-to-globals :pry :pry-light :pry-top
    :with-local-wrapper
    :wrap-rest-of-input
    :pkg-pry :pkg-pry-top
    )
  )

(in-package :local-variable-debug-wrapper)

(defvar *saved-lexenvs* nil 
  "Dynamically scoped stack of saved lexical environments")
(defvar *saved-lexenv-cursor* 0
  "A cursor for inspecting the stack of saved lexical environments")

(defmacro push-lexenv-to-saved (&environment env &body body)
  "A wrapper that obtains the current environment and saves the data from it to the stack"
  (let*
    (
     (varnames nil)
     (funcnames nil)
     )
    (when env
      (iterate-variables-in-lexenv 
        (lambda (name &key &allow-other-keys)
          (pushnew name varnames))
        env)
      (iterate-functions-in-lexenv
        (lambda (name &key &allow-other-keys)
          (pushnew name funcnames))
        env))
    (setf varnames (remove '*saved-lexenvs* varnames))
    (setf varnames (remove-if (lambda (x) (null (symbol-package x))) varnames))
    (setf 
      funcnames 
      (remove-if (lambda (x) (null (symbol-package x))) funcnames))
    (if env 
      `(let
         (
          (*saved-lexenvs*
            (cons
              (list
                ,@(when varnames
                    `(
                      :variables
                      (list
                        ,@
                        (loop
                          for varname in varnames
                          collect 
                          `(cons
                             ',varname
                             (lambda (&optional (value nil value-given))
                               (if value-given
                                 (setf ,varname value)
                                 ,varname)))))))
                ,@(when funcnames
                    `(
                      :functions
                      (list
                        ,@
                        (loop
                          for funcname in funcnames
                          collect 
                          `(cons
                             ',funcname
                             (lambda (&rest args)
                               (apply #',funcname args))
                             )
                          ))))
                )
              *saved-lexenvs*))
          )
         ,@body
         )
      `(progn ,@body))))

(defmacro local-variable (var &optional (value nil value-given))
  "Get or set the local variable from a preserved environment"
  (let*
    (
     (var-reference 
       `(cdr 
          (or
            (assoc 
              ',var 
              (getf 
                (nth *saved-lexenv-cursor* *saved-lexenvs*) 
                :variables))
            (assoc 
              ,(symbol-name var)
              (getf 
                (nth *saved-lexenv-cursor* *saved-lexenvs*) 
                :variables)
              :test 'equal :key 'symbol-name))))
     )
    `(funcall ,var-reference ,@(when value-given (list value)))))

(defmacro local-function (func &rest args)
  "Call a local function from a preserved environment"
  `(funcall 
     (cdr 
       (or
         (assoc 
           ',func 
           (getf 
             (nth *saved-lexenv-cursor* *saved-lexenvs*) 
             :functions))
         (assoc 
           ,(symbol-name func)
           (getf 
             (nth *saved-lexenv-cursor* *saved-lexenvs*) 
             :functions)
           :test 'equal :key 'symbol-name)))
     ,@args))

(defmacro locvar (&rest args)
  "An alias for local-variable"
  `(local-variable ,@args))
(defmacro locfunc (&rest args)
  "An alias for local-function"
   `(local-function ,@args))

(defun list-local-variables ()
  "List local variables in a preserved environment"
  (mapcar 'car (getf (nth *saved-lexenv-cursor* *saved-lexenvs*) :variables)))

(defun list-local-functions ()
  "List local functions in a preserved environment"
  (mapcar 'car (getf (nth *saved-lexenv-cursor* *saved-lexenvs*) :functions)))

(defun list-locvars () 
  "An alias for list-local-variables"
  (list-local-variables))
(defun list-locfuncs () 
  "An alias for list-local-functions"
  (list-local-functions))

(defun lexenv-stack-cursor-set (value &key relative)
  "Set (or increment) the cursor in the stack of preserved lexical environments"
  (setf 
    *saved-lexenv-cursor* 
    (if relative (+ *saved-lexenv-cursor* value) value))
  (when (< *saved-lexenv-cursor* 0) (setf *saved-lexenv-cursor* 0))
  (when (>= *saved-lexenv-cursor* (length *saved-lexenvs*))
    (setf *saved-lexenv-cursor* (1- (length *saved-lexenvs*))))
  *saved-lexenv-cursor*)

(defun push-lexenv-to-saved-inner (&rest args)
  "A dummy function to silence cl-walker; will get rewritten to a macro call to push-lexenv-to-saved"
  (first (last args)))

#-(or ecl clisp)
(defun wrap-node (node)
  "An intermediate function that adds push-lexenv-to-saved-inner pseudo-calls into the code whenever the lexical environment is likely to change"
  (labels
    (
     (wrap-inner 
       (parent type node)
       (wrap-body-of 
         node
         (find 
           type 
           (list
             'hu.dwim.walker::bindings
             'hu.dwim.walker::declarations
             ))
         (or
           (ignore-errors
             (bindings-of parent))
           (ignore-errors
             (name-of parent))
           )
         )
       )
     (wrap-body-of
       (node skip-level has-bindings)
       (let*
         (
          (new-node
            (rewrite-ast-links node #'wrap-inner)
            )
          (wrapper 
            (walk-form
              `(push-lexenv-to-saved-inner)
              ))
          )
         (setf 
           (arguments-of wrapper) 
           (list (copy-ast-form node)))
         (if (or skip-level (not has-bindings)) node wrapper)
         )
       )
     )
    (wrap-inner nil nil node)
    ))

           #-(or ecl clisp)
(defmacro with-local-wrapper (&rest forms)
  "Execute forms; preserve data from lexical environment where needed"
  `(macrolet
     (
      (push-lexenv-to-saved-inner 
        (&rest args)
        `(push-lexenv-to-saved ,@ args))
      )
     ,(unwalk-form
        (wrap-node
          (walk-form
            `(progn
               ,@forms))))))

(defmacro pry-light (&key reason preserve-package)
  "Launch an interactive debugging session"
  `(let ((*package* ,(if preserve-package '*package* *package*)))
     (cerror 
       "Exit PRY session and resume execution"
       (or ,reason "Lightweight PRY session invoked"))))

(defmacro with-locals-to-globals (&body body)
  "Convert local variables from a preserved environment into dynamical variables in the current dynamical extent for easier inspection"
  `(progv
     (mapcar 'car (getf (nth *saved-lexenv-cursor* *saved-lexenvs*) :variables))
     (mapcar 'funcall (mapcar 'cdr (getf (nth *saved-lexenv-cursor* *saved-lexenvs*) :variables)))
     (loop 
       for x in 
       (getf (nth *saved-lexenv-cursor* *saved-lexenvs*) :variables)
       do (proclaim (list 'special (car x))))
     ,@ body))

(defmacro pry-unwrapped (&key reason)
  "Launch an interactive debugging session and try to convert compile-time lexical environment to dynamical variables"
  `(push-lexenv-to-saved (with-locals-to-globals 
                           (pry-light :reason (or ,reason "PRY-unwrapped session invoked")))))

(defmacro pry (&key reason)
  "Launch an interactive debugging session with preserved local variables converted to dynamical ones — use compile-time lexenv if no saved lexenv available"
  `(if
     *saved-lexenvs*
     (with-locals-to-globals 
       (pry-light :reason (or ,reason "PRY session invoked")))
     (pry-unwrapped :reason (or ,reason "PRY session invoked"))))

(defmacro pry-top (&optional reason)
  "Launch an interactive debugging session with preserved local variables converted to dynamical ones; always use the top (innermost, freshest) frame"
  `(progn
     (lexenv-stack-cursor-set 0)
     (with-locals-to-globals 
       (pry-light :reason (or ,reason "Top PRY session invoked")))))

(defvar *preserved-readtable* nil
  "A variable to store current readtable when wrapping till the EOF")

(defun defmacro-form-p (f)
  "Check if the form is defmacro"
  ; checking single macroexpansion steps because SBCL (for example) defines
  ; defmacro as a macro.
  (loop
    for prev := nil then ef
    for ef := f then (macroexpand-1 f)
    when (equal prev ef) return nil
    when (equal (first ef) 'defmacro) return t))

#-(or ecl clisp)
(defun wrap-rest-of-input-reader (stream char)
  "Read a form, wrapping it into with-local-wrapper"
  (when *preserved-readtable*
    (unread-char char stream)
    (let*
      (
       (*readtable* *preserved-readtable*)
       (eof-marker (gensym))
       (form (read stream nil eof-marker))
       )
      (cond
        ((equal form eof-marker)
         (setf *preserved-readtable* nil)
         nil)
        ((defmacro-form-p form) form)
        (t `(with-local-wrapper ,form))
        ))))

#-(or ecl clisp)
(defmacro wrap-rest-of-input ()
  "Define ( as a macro-character to wrap the rest of input in with-local-wrapper"
  ; I do not want to read until ) manually, so I just unread-char the opening
  ; parenthesis
  '(eval-when
     (:compile-toplevel :load-toplevel :execute)
     (unless (equal 'wrap-rest-of-input-reader 
                    (get-macro-character #\( )) ; #\)
       (setf *readtable* (copy-readtable))
       (setf *preserved-readtable* (copy-readtable))
       (set-macro-character 
         #\( 'wrap-rest-of-input-reader
         ; #\) will work fine anyway
         ))))

(defmacro with-locals-as-pkg (&body body)
  `(let*
     (
      (package-name (gensym))
      (package (make-package package-name :use ()))
      (saved-lexenv (elt *saved-lexenvs* *saved-lexenv-cursor*))
      )
     (do-symbols 
       (s *package*)
       (if
         (or
           (special-operator-p s)
           (eq '*package* s)
           )
         (import s package)
         (let*
           (
            (sn (intern (symbol-name s) package))
            )
           (setf (symbol-plist sn) (symbol-plist s))
           (when (boundp s)
             (setf (symbol-value sn) (symbol-value s)))
           (when (fboundp s)
             (if (macro-function s)
               (setf (macro-function sn) (macro-function s))
               (setf (symbol-function sn) (symbol-function s)))))))
     (loop
       for var in (getf saved-lexenv :variables)
       for local-sym := (intern (symbol-name (car var)) package)
       do 
       (setf 
         (symbol-value local-sym) 
         (funcall (cdr var)))
       unless (eq '*package* (car var)) do
       (proclaim (list 'special local-sym)))
     (loop
       for func in (getf saved-lexenv :functions)
       do 
       (setf 
         (symbol-function (intern (symbol-name (car func)) package)) 
         (cdr func)))
     (let*
       (
        (*package* package)
        )
        ,@ body
       )
     ))

(defmacro pkg-pry (&key (reason "Packaged-PRY session invoked")
                        preserve-package)
  "Invoke a PRY session using a fresh package to provide access to local
  functions and variables"
  `(let
     ((*package* ,(if preserve-package '*package* *package*)))
     (if *saved-lexenvs*
       (with-locals-as-pkg
         (pry-light :reason ,reason :preserve-package t))
       (push-lexenv-to-saved
         (with-locals-as-pkg
           (pry-light :reason ,reason :preserve-package t)))
       )))

(defmacro pkg-pry-top (&key (reason "Top Packaged-PRY session invoked")
                            preserve-package)
  "Invoke a PRY session using a fresh package to provide access to local
  functions and variables; ensure it uses the top of the stack"
  `(let
     ((*saved-lexenv-cursor* 0)
      (*package* ,(if preserve-package '*package* *package*)))
     (if *saved-lexenvs*
       (with-locals-as-pkg
         (pry-light :reason ,reason :preserve-package t))
       (push-lexenv-to-saved
         (with-locals-as-pkg
           (pry-light :reason ,reason :preserve-package t)))
       )))
