(use-package :local-variable-debug-wrapper)

; This has to be outside of the wrapper due to hu.dwim.walker limitation
(defmacro test-macro (x)
  `(let*
     ((z 42)
      (y ,x))
     (+ 3 y)
     ))

(with-local-wrapper

  (defun test-m-1 ()
    (test-macro (progn (pry) 7)))

  )

(wrap-rest-of-input)

; This will be skipped automatically
(defmacro test-macro-2 (x)
  `(let*
     ((z 777)
      (y ,x))
     (+ 3 y)
     ))

(defun test-m-2 ()
  (test-macro-2 (progn (pry) 7)))
