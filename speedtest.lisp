(use-package :local-variable-debug-wrapper)

(defparameter *pry-on-bottom* nil)

(defun fib-uw (n)
  (if (<= n 1) (progn (when *pry-on-bottom* (pry)) 1) (+ (fib-uw (- n 1)) (fib-uw (- n 2)))))

(defun mfib-uw (n)
  (if (<= n 1) (progn (when *pry-on-bottom* (pry)) 1) (+ 1 (mfib-uw (- n 1)) (mfib-uw (- n 2)))))

(wrap-rest-of-input)

(defun fib-w (n)
  (if (<= n 1) (progn (when *pry-on-bottom* (pry)) 1) (+ (fib-w (- n 1)) (fib-w (- n 2)))))

