(asdf:defsystem 
  :local-variable-debug-wrapper

  ; It is more of a debugging tool than of a library
  :licence "GPLv3+"

  :description 
  "A tool to ensure access to lexical local variables in REPL"

  :depends-on 
  (
   #-(or ecl clisp) :hu.dwim.walker
   )

  :components 
  (
   (:static-file "README")
   (:static-file "AUTHORS")
   (:static-file "COPYING")
   (:static-file "gpl-3.0.txt")
   (:file "lexenv")
   #+(or ccl) (:file "ccl-lexenv")
   (:file "wrap")
   ))
