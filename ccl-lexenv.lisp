;;; -*- mode: Lisp; Syntax: Common-Lisp; -*-
;;; Copyright (c) 2002-2006, Edward Marco Baringer
;;; Copyright (c) 2008, Attila Lendvai
;;; Copyright (c) 2016, Michael Raskin, Shviller
;;; All rights reserved.
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;; - Redistributions of source code must retain the above copyright
;;; notice, this list of conditions and the following disclaimer.
;;; 
;;; - Redistributions in binary form must reproduce the above copyright
;;; notice, this list of conditions and the following disclaimer in the
;;; documentation and/or other materials provided with the distribution.
;;; 
;;; - Neither the name of the copyright holders, nor BESE, nor the names
;;; of its contributors may be used to endorse or promote products derived
;;; from this software without specific prior written permission.
;;; 
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;; A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;
;;; Copyright (c) 2009 by the authors.

(in-package :local-variable-debug-wrapper-lexenv-parser)

;;;
;;; Clozure CL
;;;

(defun make-empty-lexical-environment ()
  ;; Tries to pick up a defenv reference from the special
  ;; variable, but don't count on it always working.
  (ccl::new-lexical-environment
   (ccl::definition-environment ccl::*nx-lexical-environment*)))

;;;
;;; utilities
;;;

(defun ccl-defenv-p (env)
  (ccl::istruct-typep env 'ccl::definition-environment))

(defmacro do-ccl-env-chain ((env-var env-item &key with-defenv) &body code)
  `(do ((,env-var ,env-item (ccl::lexenv.parent-env ,env-var)))
       ((or (null ,env-var)
            ,(if with-defenv
                 `(consp ,env-var)
                 `(ccl-defenv-p ,env-var))))
     ,@code))

(defun ccl-get-env-vars (env)
  ;; The variable list field may contain a special
  ;; barrier sentinel. Ignore it.
  (let ((lst (ccl::lexenv.variables env)))
    (if (listp lst) lst)))

(defun proclaimed-special-variable?/global (name lexenv)
  ;; During compilation the special proclamations are
  ;; collected in the definition environment.
  ;; This means that UNDER NO CIRCUMSTANCES is this
  ;; function to be changed to a lexenv-less form.
  (or (ccl:proclaimed-special-p name)
      (let* ((defenv (if (and lexenv (ccl-defenv-p lexenv))
                         lexenv
                         (ccl::definition-environment lexenv)))
             (specials (if defenv (ccl::defenv.specials defenv))))
        (ccl::assq name specials))))

(defun ccl-find-var-decl (name type decls)
  (cdr (find-if (lambda (item)
                  (and (eq (first item) name)
                       (eq (second item) type)))
                decls)))

(defun ccl-defined-const-p (name &optional lexenv)
  (let* ((defenv (ccl::definition-environment lexenv))
         (consts (if defenv (ccl::defenv.constants defenv))))
    (ccl::assq name consts)))

(defun ccl-symbol-macro-p (var-spec)
  (let ((exp (ccl::var-expansion var-spec)))
    (and (consp exp)
         (eql :symbol-macro (car exp)))))

;;;
;;; iteration
;;;

(defun iterate-variables-in-lexenv (visitor lexenv
                                            &key include-ignored? (include-specials? t) include-macros?)
  (let* ((defenv (ccl::definition-environment lexenv))
         (hide-list ()))
        (do-ccl-env-chain (env lexenv :with-defenv t)
          ;; Local functions spawn temporaries; hide them
          (dolist (func-spec (ccl::lexenv.functions env))
            (when (eql 'ccl::function (cadr func-spec))
              (push (cdddr func-spec) hide-list)))
          ;; Handle the environment layer
          (if (ccl-defenv-p env)
            (progn
              (when include-macros?
                (dolist (cell (ccl::defenv.symbol-macros env))
                  (funcall visitor (car cell) :macro? t))))
            ;; Lexical environment
            (let* ((decls         (ccl::lexenv.vdecls env))
                   (special-decls (remove 'special decls :key #'cadr :test-not #'eq)))
              ;; Walk vars
              (dolist (var-spec (ccl-get-env-vars env))
                (let* ((name      (ccl::var-name var-spec))
                       (macro?    (ccl-symbol-macro-p var-spec))
                       (ignored?  (cdr (ccl-find-var-decl name 'ignore decls)))
                       (special?  (or (find name special-decls :key #'car)
                                      (proclaimed-special-variable?/global name defenv))))
                  (when special?
                    (setf special-decls (remove name special-decls :key 'car)))
                  (if macro?
                    (when include-macros?
                      (funcall visitor name :macro? t))
                    (when (not (member name hide-list))
                      (funcall visitor name :ignored? ignored? :special? special? :macro? nil)))))
              ;; Enumerate var-less special decls as vars
              (dolist (decl special-decls)
                (funcall visitor (first decl) :special? t :ignored? nil :macro? nil)))))))

(defun iterate-functions-in-lexenv (visitor lexenv &key include-macros?)
  (do-ccl-env-chain (env lexenv :with-defenv t)
    ;; lexenv.functions can operate on a defenv
    (dolist (func-spec (ccl::lexenv.functions env))
      (let* ((name      (ccl::maybe-setf-name (first func-spec)))
             (function? (eql 'ccl::function (second func-spec)))
             (macro?   (eql 'ccl::macro (second func-spec))))
        (when (and macro? include-macros?)
          (assert (functionp (cddr func-spec)))
          (funcall visitor name :macro? t))
        (when function?
          (funcall visitor name :macro? nil))))))

(defun iterate-blocks-in-lexenv (visitor lexenv)
  (declare (ignore visitor lexenv))
  (cerror "ignore and do nothing"
          "The lexical environment does not contain blocks in Clozure CL"))

(defun iterate-tags-in-lexenv (visitor lexenv)
  (declare (ignore visitor lexenv))
  (cerror "ignore and do nothing"
          "The lexical environment does not contain tags in Clozure CL"))

;;;
;;; augmentation
;;;

(defun augment-lexenv-with-variable (name lexenv &key special ignored)
  (let* ((decls (if special `((special ,name))))
         (env (ccl:augment-environment lexenv :variable (list name) :declare decls)))
    ;; augment-environment does not understand ignore decls
    (when ignored
      (push (list* name 'ignore t) (ccl::lexenv.vdecls env)))
    env))

(defun augment-lexenv-with-function (name lexenv)
  (ccl:augment-environment lexenv :function (list name)))

(defun augment-lexenv-with-macro (name def lexenv)
  (ccl:augment-environment lexenv :macro (list (list name def))))

(defun augment-lexenv-with-symbol-macro (name def lexenv)
  (ccl:augment-environment lexenv :symbol-macro (list (list name def))))

(defun augment-lexenv-with-block (name lexenv)
  (declare (ignore name))
  ;; Do nothing
  lexenv)

(defun augment-lexenv-with-tag (name lexenv)
  (declare (ignore name))
  ;; Do nothing
  lexenv)

