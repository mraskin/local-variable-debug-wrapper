(defpackage 
  :local-variable-debug-wrapper-lexenv-parser
  (:use :common-lisp)
  (:export
    #:iterate-variables-in-lexenv
    #:iterate-functions-in-lexenv
    ))

(in-package :local-variable-debug-wrapper-lexenv-parser)

#+sbcl (progn
(defun iterate-variables-in-lexenv (callback env &key include-macros?)
  (loop
    for entry in (sb-c::lexenv-vars env)
    for name := (car entry)
    for attrs := (cdr entry)
    for specialp := (typep attrs 'sb-c::global-var)
    for ignoredp := (ignore-errors (sb-c::lambda-var-ignorep attrs))
    for macrop := (and (consp attrs) (eq 'sb-sys::macro (car attrs)))
    when (or include-macros? (not macrop))
    do (funcall callback name :ignored? ignoredp :special? specialp :macro? macrop)))
(defun iterate-functions-in-lexenv (callback env &key include-macros?)
  (loop
    for entry in (sb-c::lexenv-funs env)
    for name := (car entry)
    for attrs := (cdr entry)
    for macrop := (and (consp attrs) (eq 'sb-sys::macro (car attrs)))
    when (or include-macros? (not macrop))
    do (funcall callback name :macro? macrop)))
)

#+ecl (progn
(defvar *special-marker* nil)
(macrolet 
  ((extract 
     (&environment env)
     (setf 
       *special-marker* 
       (second 
         (find 
           'y 
           (if (fboundp 'c::cmp-env-variables)
             (c::cmp-env-variables env) 
             (car env))
           :key 'first)))
     nil))
  (let ((y 1)) (declare (special y)) (extract)))
(defun iterate-variables-in-lexenv (callback env &key include-macros?)
  (loop
    for entry in 
    (if (fboundp 'c::cmp-env-variables)
      (c::cmp-env-variables env)
      (car env))
    for name := (car entry)
    for specialp := (eq (second entry) *special-marker*)
    for ignoredp := nil
    for macrop := (eq 'si::symbol-macro (second entry))
    for functionp := (not (third entry))
    when (or include-macros? (not macrop))
    unless functionp
    do (funcall callback name :ignored? ignoredp :special? specialp :macro? macrop)))
(defun iterate-functions-in-lexenv (callback env &key include-macros?)
  (loop
    for entry in 
    (if (fboundp 'c::cmp-env-functions)
      (c::cmp-env-functions env)
      (cdr env))
    for name := (car entry)
    for macrop := (eq 'si::macro (second entry))
    when (or include-macros? (not macrop))
    do (funcall callback name :macro? macrop)))
)

#+ccl (progn
; defined in ccl-lexenv.lisp
)

#+clisp (progn
(defvar *symbol-macro-type* 
  (macrolet ((f (&environment e) `',(type-of (elt (elt e 0) 1)))) 
    (symbol-macrolet ((x 1)) (f))))
(defvar *special-variable-type* 
  (macrolet ((f (&environment e) `',(type-of (elt (elt e 0) 1)))) 
    (let ((x 1)) (declare (special x)) (f))))
(defvar *macro-type* 
  (macrolet ((f (&environment e) `',(type-of (elt (elt e 1) 1)))) 
    (f)))
(defun iterate-variables-in-lexenv (callback environment &key include-macros?)
  (when (aref environment 0)
    (loop
      with seen := (make-hash-table)
      with arr := (aref environment 0)
      with cursor := 0
      for current := (aref arr cursor)
      for nested := (typep current 'array)
      ; CLISP does interesting things with symbol-macros
      ; (defun f () 
      ;   (macrolet ((f (&environment e) `',(elt (elt e 0) 1))) 
      ;     (symbol-macrolet ((x t)) (f)))) 
      ; (let ((x (f))) (setq x nil))
      for val := (lambda () (and current (not nested) (elt arr (1+ cursor))))
      for macrop := (equal (type-of (funcall val)) *symbol-macro-type*)
      for specialp := (equal (type-of (funcall val)) *special-variable-type*)
      for ignoredp := nil
      for seenp := (gethash current seen)
      unless current return nil
      do 
      (cond 
        (nested (setf arr current cursor 0))
        (seenp (incf cursor 2))
        ((or include-macros? (not macrop))
         (funcall 
           callback current 
           :ignored? ignoredp :special? specialp :macro? macrop)
         (setf (gethash current seen) t)
         (incf cursor 2))
        (t (incf cursor 2))
        )
      ))
  )
(defun iterate-functions-in-lexenv (callback environment &key include-macros?)
  (when (aref environment 1)
    (loop
      with seen := (make-hash-table)
      with arr := (aref environment 1)
      with cursor := 0
      for current := (aref arr cursor)
      for nested := (typep current 'array)
      for val := (and current (not nested) (elt arr (1+ cursor)))
      for macrop := (equal (type-of val) *macro-type*)
      for seenp := (gethash current seen)
      unless current return nil
      do 
      (cond 
        (nested (setf arr current cursor 0))
        (seenp (incf cursor 2))
        ((or include-macros? (not macrop))
         (funcall callback current :macro? macrop)
         (setf (gethash current seen) t)
         (incf cursor 2))
        (t (incf cursor 2))
        )
      ))
  )
)

(unless (fboundp 'iterate-variables-in-lexenv)
  (defun iterate-variables-in-lexenv (&rest args)
    (declare (ignore args)))
  (defun iterate-functions-in-lexenv (&rest args)
    (declare (ignore args)))
  )
