(load "lexenv.lisp")
#+(or ccl) (load "ccl-lexenv.lisp")

(use-package :local-variable-debug-wrapper-lexenv-parser)

(defmacro test-lexenv (&environment env)
  (iterate-variables-in-lexenv 
    (lambda (name &rest form &key &allow-other-keys) 
      (format t "Variable: ~s | ~s~%" name form)) 
    env :include-macros? t)
  (iterate-functions-in-lexenv 
    (lambda (name  &rest form &key &allow-other-keys) 
      (format t "Function: ~s | ~s~%" name form)) 
    env :include-macros? t))

(defvar *global* 1)
(defvar *local-global* 2)

(let
  ((x 3) (*local-global* 3))
  (labels 
    ((f () 1))
    (symbol-macrolet
      ((s 77))
      (macrolet
        ((m () 678))
        (lambda (y z u)
          (declare (ignorable y) (ignore z))
          (test-lexenv) x u)))))

(macrolet ((test (&environment env) `',env)) (let ((*local-global* 1)) (labels ((f ())) (test))))
