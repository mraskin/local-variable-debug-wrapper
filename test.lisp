(use-package :local-variable-debug-wrapper)

(with-local-wrapper

  (defun test-0 ()
    (labels 
      ((f (x) (+ x 1)))
      (f 3)))

  (defun test-1 ()
    (labels 
      ((f (x) (+ x 1)))
      (error "Oops")))

  (defun test-2 ()
    (let
      ((x 1))
      (error "Oops")))

  (defun test-3 ()
    (let ((x 3) (y 4))
      (let ((z 5) (u (pry "Oooops")))
        (+ x y z))))

  (defun test-4 ()
    (let* ((x 7) (y (error "Oops"))) 3))

  (defun test-5 ()
    (labels ((f () 1))
      (labels ((g () 2))
        (let ((x 1))
          (let ((y 2))
            (error "Oops"))))))

  (defun test-6 ()
    (let ((x 1))
      (pry)))

  ; we do not copy data, so destrutive modifications are propagated up through
  ; the stack
  ;
  ; we can have two different x
  (defun test-7 ()
    (let*
      (
       (f 
         (let* 
           (
            (x 1)
            )
           (labels
             (
              (g () (incf x))
              )
             (lambda (y) (setf x (1+ y)) (g) (let ((z 7)) (incf x z) (pry) x))
             )
           ))
       (h 
         (let* 
           (
            (x 77)
            )
           (labels
             (
              (k () (let ((x (* x 2))) x))
              )
             (lambda (y) 
               (setf x y)
               (let* ((x (k))) (let* ((x (* x 3))) (funcall f x))))
             )
           ))
       )
      (funcall h 1023)
      )
    )

  (defun test-8 ()
    (loop
      for x from 1 to 1
      for y := (+ x 2)
      do (pry)
      collect (+ x y)
      ))

  )

; Pry without wrapping
(defun test-uw-1 ()
  (let ((x 1))
    (let ((y 2))
      (pry))))

; Wrapping to the end of file
(wrap-rest-of-input)

(defun test-w-1 ()
  (let ((x 1)) (pry)))

(defun test-w-2 ()
  (let ((x 2)) (pry)))

(defun test-w-! ()
  (let ((x 3)) (pry)))

; Even that doesn't break much
#.(wrap-rest-of-input)

(defun test-w-!! ()
  (let ((x 3)) 
    (let ((y 4))
      (pry))))

(defparameter *test-pkg-pry-1-private-global* 17)

(defparameter *test-pkg-pry-1-shadowed-global* 71)

(defun test-pkg-pry-1 ()
  (let*
    (
     (x 1)
     (y 789)
     (*test-pkg-pry-1-shadowed-global* 887)
     )
    (labels
      (
       (labels-f (x) (* 2 x))
       )
      (flet
        (
         (flet-f (x) (* 3 x))
         )
        (pkg-pry)
        67)))
  *test-pkg-pry-1-shadowed-global*)
