(require :cffi-examples)

(defpackage :monitor)

(defmacro monitor::get-time ()
  `(the unsigned-byte
        (multiple-value-bind
          (sec usec)
          (cffi-example-gettimeofday:gettimeofday)
          (+ (* 1000000 sec) usec))))

(defconstant monitor::time-units-per-second 1000000)

(defvar monitor::*external-get-time* t)

(require :metering)

#+(or sbcl ccl)(require :local-variable-debug-wrapper)
#+(or ecl clisp)
(defpackage :local-variable-debug-wrapper 
  (:export #:wrap-rest-of-input))
#+(or ecl clisp)
(defun local-variable-debug-wrapper:wrap-rest-of-input ())

(load "speedtest.lisp")

(defmacro mapdefun (f l)
  `(progn
     ,@(loop
         for x in l
         for sym :=
         (intern 
           (format
             nil "~a-~a-~a"
             f 
             (substitute 
               #\_ #\Space
               (string-upcase 
                 (lisp-implementation-type)))
             x))
         collect
         `(defun 
            ,sym
            ()
            (,f ,x))
         collect
         `(monitor:monitor ,sym))
     (defun ,(intern (format nil "~a-~a" f 'all)) ()
       (progn
         ,@(loop
             for x in l
             collect
             (list
               (intern
                 (format
                   nil "~a-~a-~a"
                   f 
                   (substitute 
                     #\_ #\Space
                     (string-upcase 
                       (lisp-implementation-type)))
                   x))))))))

(mapdefun fib-uw (0 5 10 15 20 25 30 35 40))
(mapdefun fib-w  (0 5 10 15 20 25 30 35 40))

(monitor:reset-all-monitoring)

(fib-w-all )
(fib-uw-all)

(monitor:report-monitoring :all :inclusive 0)
