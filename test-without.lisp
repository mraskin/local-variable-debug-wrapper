(declaim 
  (optimize (debug 3) (speed 0)  
    (safety 3) (space 0) (compilation-speed 0)))

(defun test-wo-1 ()
  (labels ((f (z) (+ z 1)))
    (let ((x 2))
      (cerror "Continue" "Error invoked")
      (f x))))

(defun test-wo-2 ()
  (loop
    for x from 1 to 1
    for y := (+ x 2)
    do (cerror "Continue" "Error invoked")
    collect (+ x y)
    ))

(defun test-wo-3 ()
  (loop
    for x from 1 to 1
    for y := (+ x 2)
    do (cerror "Continue" "Error invoked")
    collect (+ x y)
    collect (* x y)
    ))

(defun test-wo-4 ()
  (labels ((f (z) (+ z 1 (* z (+ z 17) 7) (expt 2 z))))
    (let ((x 2))
      (cerror "Continue" "Error invoked")
      (+ (f x) (f (* x 2))))))

